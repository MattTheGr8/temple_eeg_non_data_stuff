import keras
import scipy.io
import hdf5storage

model_file  = [ 'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0000_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0001_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0002_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0003_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0004_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0005_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0006_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0007_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0008_fold0000.h5',
                'results/ptc_ibmseiz_newcat_simplecnn_train_1234_trained_model_iter0009_fold0000.h5'
              ]
data_file   = [ 'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype1.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype2.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype3.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype4.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype5.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype6.mat',
                'testdata/ptc_ibmseiz_newcat_train_1234_test_567_testdata_seiztype7.mat'
              ]
output_file_stem = 'results/ptc_ibmseiz_newcat_testresults_train_1234_test_567_'

for iter in range(10):
    for seiztype in range(7):
        trained_model = keras.models.load_model(model_file[iter])
        matdata = hdf5storage.loadmat(data_file[seiztype])
        test_dat = matdata['test_dat']
        test_st1 = matdata['test_st1']
        test_st2 = matdata['test_st2']
        test_ft1 = matdata['test_ft1']
        test_ft2 = matdata['test_ft2']
        preds = trained_model.predict(test_dat)
        iter_str = 'iter{0:04d}_'.format(iter)
        seiz_str = 'seiztype{0}'.format(seiztype+1)
        output_file = output_file_stem + iter_str + seiz_str + '.mat'
        outdict = {}
        outdict['preds'] = preds
        outdict['test_st1'] = test_st1
        outdict['test_st2'] = test_st2
        outdict['test_ft1'] = test_ft1
        outdict['test_ft2'] = test_ft2
        scipy.io.savemat(output_file, outdict)
