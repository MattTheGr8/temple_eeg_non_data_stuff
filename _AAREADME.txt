File: _AAREADME.txt
Database: IBM TUSZ Pre-Processed Data (IBMPPD)
Version: 1.0.0

DETAILS OF THE DATA:

This dataset represents a preprocessed version of the TUH Seizure Detection
Corpus (TUSZ). We request you to go through the following paper
(https://arxiv.org/abs/1902.01012) to get details about the dataset
and benchmark results using machine- and deep learning algorithms. The
dataset contains 2012 seizures and 8 seizure types. As described in
the paper, we pre-processed the dataset using two different techniques
which can be found in the data folder.

pp_1 and pp_2 corresponds to pre-processing method 1 and method 2
discussed in the paper.  For both pre-processing techniques, each
seizure is stored in a seperate pickle file. For each seizure in pp_1,
the first, second, and third row corresponds to the number of samples,
number of channels, and number of frequency bands respectively. On the
other hand, for each seizure in pp_2, the first and second row
corresponds to the number of samples and features respectively. For
both the pre-processing technique, the parameters used are taken from
the best performing row of Table 2 of the paper.

HOW TO LOAD THE DATA?

Each seizure is a namedtupe of the following format:

 seizure_type_data = collections.namedtuple('seizure_type_data', ['seizure_type', 'data'])

Please define this namedtuple by using the above snippet in any code
that you write. Once it is done, you can load any seizure by using the
following code snippet:

 temp = pickle.load(open(file_path, 'rb'))
 seizure_data = temp.data #The pre-processed seizure data
 seizure_type = temp.seizure_type #The seizure type

If you have any questions about the data, please contact:

 Subhrajit Roy
 Research Scientist - Artificial Intelligence/Machine Learning
 IBM Research, Melbourne, Australia
 Email: subhrajit.roy@au1.ibm.com

Acknowledgements:

If you use this pre-processed dataset in your work please cite the
following two papers:

1. S. Roy et al., "Machine Learning for Seizure Type Classification:
Setting the benchmark", arXiv:1902.01012, Jan. 2019.

2. V. Shah et al., "The Temple University Hospital Seizure Detection
Corpus", Frontiers in Neuroinformatics, vol. 12, 2018.

Link to the dataset:
 www.isip.piconepress.com/projects/tuh_eeg/downloads/ibm_seizure_preprocessed/

