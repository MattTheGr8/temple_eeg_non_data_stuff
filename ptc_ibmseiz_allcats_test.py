import keras
import scipy.io
import hdf5storage

model_file_format  = 'results/ptc_ibmseiz_samediff_simplecnn_allcats_set{0:03d}_trained_model_iter{1:04d}_fold0000.h5'
data_file_format   = 'testdata/ptc_ibmseiz_samediff_allcats_testdata_set{0:03d}.mat'
output_file_stem = 'results/ptc_ibmseiz_allcats_testresults_'

for set in range(1,13):
    data_file = data_file_format.format(set)
    print('Loading: ' + data_file)
    matdata = hdf5storage.loadmat(data_file)
    test_dat = matdata['test_dat']
    test_st1 = matdata['test_st1']
    test_st2 = matdata['test_st2']
    test_ft1 = matdata['test_ft1']
    test_ft2 = matdata['test_ft2']
    test_ift = matdata['test_item_ft']
    test_ist = matdata['test_item_st']
    set_str  = 'set{0:03d}_'.format(set)
    
    for iter in range(10):
        model_file = model_file_format.format(set, iter)
        print('- predicting: ' + model_file)
        trained_model = keras.models.load_model(model_file)
        preds = trained_model.predict(test_dat)
        iter_str = 'iter{0:04d}'.format(iter)
        output_file = output_file_stem + set_str + iter_str + '.mat'
        outdict = {}
        outdict['preds'] = preds
        outdict['test_st1'] = test_st1
        outdict['test_st2'] = test_st2
        outdict['test_ft1'] = test_ft1
        outdict['test_ft2'] = test_ft2
        outdict['test_item_ft'] = test_ift
        outdict['test_item_st'] = test_ist
        scipy.io.savemat(output_file, outdict)
