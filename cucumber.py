import pickle
import glob
import scipy.io

pickles = glob.glob("*.pkl")
for f in pickles:
    d = pickle.load( open(f,'rb') )
    out = {}
    out['seiz_type'] = d.seizure_type
    out['seiz_data'] = d.data
    outname = f.replace('pkl','mat')
    scipy.io.savemat(outname, out)

